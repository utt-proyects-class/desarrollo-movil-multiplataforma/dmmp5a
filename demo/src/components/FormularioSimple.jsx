import React from "react";


const validate = values => {
    const errors = {}
    if (!values.username) {
        errors.username = "campoObligatorio"
    } 
    if (!values.email) {
        errors.email = "campoObligatorio"
    }     
    return errors
}
export default class FormularioSimple extends React.Component{
    state = {
        errors: {
        
        } 
    }

    handleChange = ({target}) => {
        const {name, value} = target
        this.setState({ [name]: value })
    }

    handleSubmit = (e) => {
        e.preventDefault()
        console.log("Prevenido!")
        const {errors, ...sinErrors} = this.state
        console.log(sinErrors)
        console.log(errors)
        const results = validate(sinErrors)
        this.setState({ errors: results})
        if (!Object.keys(results).length) {
            // Enviar el formulario
            console.log("Formulario Valido")
            e.target.reset()
        }
    }

    render(){
        console.log(this.state)
        const {errors} = this.state

        return(
            <form onSubmit={this.handleSubmit}>
                <input type="text" name="username" onChange={this.handleChange}/>
                    {errors.username && <p>{errors.username}</p>}
                <input type="text" name="email" onChange={this.handleChange}/>
                    {errors.email && <p>{errors.email}</p>}
                <input type="submit" value="Aceptar"/>
            </form>
        )
    }
}