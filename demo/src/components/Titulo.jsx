import React from "react";

export default class Titulo extends React.Component{
    render(){
        return(
            <h1>{this.props.titulo}</h1>
        )
    }
}