import './App.css';
import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css'
import {
  Table,
  Container,
  Button,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  FormGroup,
  Label,
  Input
} from 'reactstrap'

const data = [
  {id: 1, character: "Kakashi Hatake", anime: "Naruto"},
  {id: 2, character: "Monke D, Luffy", anime: "One Piece"},
  {id: 3, character: "Ikki Fenix", anime: "Saint Seya"},
  {id: 4, character: "Bills Dgod", anime: "DBS"},
  {id: 5, character: "Ricardo Martinez", anime: "Hajime no Ippo"},
] 

export default class App extends React.Component {
  constructor(){
    super()
  }

  state = {
    data: data,
    modalActualizar: false,
    modalInsertar: false,
    form: {
      id: "",
      character: "",
      anime: ""
    }
  }
  
  handleChange = e =>{
    this.setState({
      form: {
        ...this.state.form,
        [e.target.name]: e.target.value 
      }
    });
  }

  delete = (dato) =>{
    var opcion = window.confirm("Estás Seguro que deseas Eliminar el elemento "+dato.id);
    if (opcion == true) {
      var contador = 0;
      var arreglo = this.state.data;
      arreglo.map((registro) => {
        if (dato.id == registro.id) {
          arreglo.splice(contador, 1);
        }
        contador++;
      });
      this.setState({ data: arreglo, modalActualizar: false });
    }
  }

  update = (char) =>{
    var cont = 0
    var newdata = this.state.data
    newdata.map((registro) =>{
      if (char.id==registro.id) {
        newdata[cont].character = char.character
        newdata[cont].anime = char.anime
      }
      cont++
    })
    this.setState({modalActualizar: false, data: newdata})
  }

  insert = (dato) =>{
    var newchar = {...this.state.form}
    newchar.id = this.state.data.length + 1
    var newdata = this.state.data
    newdata.push(newchar)
    this.setState({modalInsertar: false, data: newdata})
  }

  mostrarModalInsertar = () =>{
    this.setState({modalInsertar: true})
  }

  mostrarModalActualizar = (registro) =>{
    this.setState({modalActualizar: true, form: registro})
  }

  cerrarModalActualizar = () =>{
    this.setState({modalActualizar: false})
  }

  cerrarModalInsert = () =>{
    this.setState({modalInsertar: false})
  }

  render(){
  
  return (
    <div className="App">
      <br></br>
      <br></br>
       <Container>
          <Button color='outline-success' onClick={()=>{this.mostrarModalInsertar()}}>New Character</Button>
          <br></br>
          <hr></hr>
          <Table>
              <thead>
                <tr>
                  <th>ID</th>
                  <th>CHARACTER</th>
                  <th>ANIME</th>
                  <th></th>
                  <th>ACTIONS</th>
                  <th></th>
                </tr> 
              </thead>
              <tbody>
                {this.state.data.map((element)=>(
                    <tr>
                      <th>{element.id}</th>
                      <th>{element.character}</th>
                      <th>{element.anime}</th>
                      <th><Button color='outline-primary'>Detail</Button></th>
                      <th><Button color='outline-warning' onClick={()=>{this.mostrarModalActualizar(element)}}>Update</Button></th>
                      <th><Button color='outline-danger' onClick={() => {this.delete(element)}}>Delete</Button></th>
                    </tr>
                ))}
              </tbody>
          </Table>
       </Container>

{/* Ventan Modal para Actualizar Characters */}

      <Modal isOpen={this.state.modalActualizar}>
          <ModalHeader>
            <div>
              <h3>Actualizar Character</h3>
            </div>
          </ModalHeader>

          <ModalBody>
            <FormGroup>
              <label>ID</label>
              <input className='form-control' name='id' readOnly type='text' value={this.state.form.id}></input>
            </FormGroup>
            <FormGroup>
              <label>CHARACTER</label>
              <input className='form-control' name='character'  type='text'
              onChange={this.handleChange} 
              value={this.state.form.character}></input>
            </FormGroup>
            <FormGroup>
              <label>ANIME</label>
              <input className='form-control' name='anime' type='text'
              onChange={this.handleChange} 
              value={this.state.form.anime}></input>
            </FormGroup>
          </ModalBody>

          <ModalFooter>
            <Button color='outline-warning' onClick={() => this.update(this.state.form)}>Update</Button>
            <Button color='outline-danger' onClick={() => this.cerrarModalActualizar()}>Cancel</Button>
          </ModalFooter>
      </Modal>

      {/* Fin de la Ventana Modal Actualizar */}
      
      {/* Ventana Modal para insertar nuevos Characters */}
      <Modal isOpen={this.state.modalInsertar}>
          <ModalHeader>
            <div>
              <h3>Insertar Character</h3>
            </div>
          </ModalHeader>

          <ModalBody>
            <FormGroup>
              <label>ID</label>
              <input className='form-control' name='id' type='text' readOnly value={this.state.data.length + 1}></input>
            </FormGroup>
            <FormGroup>
              <label>CHARACTER</label>
              <input className='form-control' name='character' type='text'
              onChange={this.handleChange}></input>
            </FormGroup>
            <FormGroup>
              <label>ANIME</label>
              <input className='form-control' name='anime' type='text'
              onChange={this.handleChange}></input>
            </FormGroup>
          </ModalBody>

          <ModalFooter>
            <Button color='outline-success' onClick={() => this.insert(this.state.form)}>Insertar</Button>
            <Button color='outline-danger' onClick={() => this.cerrarModalInsert()}>Cancel</Button>
          </ModalFooter>
      </Modal>
      {/* Fin de la Ventana Modal para insert nuevos Characters */}
    </div>
  );
}
}


